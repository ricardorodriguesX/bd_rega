CREATE SCHEMA IF NOT EXISTS sistema_rega_bd;

USE sistema_rega_bd;

CREATE TABLE IF NOT EXISTS t_plantas (
	id_planta INTEGER PRIMARY KEY AUTO_INCREMENT,
    humidade_min INTEGER DEFAULT 0,
	nome VARCHAR(60) NOT NULL
);

CREATE TABLE IF NOT EXISTS t_registos(
	id_registo INTEGER PRIMARY KEY AUTO_INCREMENT,
    id_planta INTEGER NOT NULL,
    humidade_ar DOUBLE DEFAULT NULL,
    humidade_solo DOUBLE DEFAULT NULL,
    temperatura DOUBLE DEFAULT NULL,
    rega INT DEFAULT 0,
    data_hora DATETIME,
    FOREIGN KEY (id_planta) REFERENCES t_plantas(id_planta)
);
